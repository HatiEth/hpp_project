#ifndef PLATFORM_CL_H
#define PLATFORM_CL_H

#include "platform.h"
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS
#include <CL/opencl.h>
#include <iostream>
#include <fstream>
#include <string.h>

#include <vector>


#define CL_USE_CPU 1



pw_internal inline
void clCheckError(cl_int Error)
{
    if(Error != CL_SUCCESS)
    {
        std::cerr << "OpenCL call failed with error " << Error << std::endl;
        std::exit(1);
    }
}

pw_internal inline
std::string LoadKernel(const char* Filepath)
{
    std::ifstream istr(Filepath);
    assert(istr);
    std::string Result(
                (std::istreambuf_iterator<char>(istr)),
                std::istreambuf_iterator<char>());
    return(Result);
}

#endif // PLATFORM_CL_H

