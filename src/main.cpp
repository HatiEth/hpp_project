#include "platform_cl.h"
#include "platformlayer.h"
#include <vector>
#include <iostream>
#include <cassert>

#include <GL/glew.h>
#include <glm/ext.hpp>

#include <ctime>


int main(int argc, char** argv)
{
    std::setbuf(stdout, NULL);

    //@Init
    cl_uint PlatformIdCount = 0;
    clGetPlatformIDs(0, nullptr, &PlatformIdCount);
    std::vector<cl_platform_id> PlatformIds(PlatformIdCount);
    clGetPlatformIDs(PlatformIdCount, PlatformIds.data(), nullptr);
    printf("Found %u Platforms\n", PlatformIdCount);

    cl_uint DeviceIdCount = 0;

#if CL_USE_CPU
    clGetDeviceIDs(PlatformIds[0], CL_DEVICE_TYPE_CPU, 0, nullptr, &DeviceIdCount);
    std::vector<cl_device_id> DeviceIds(DeviceIdCount);
    clGetDeviceIDs(PlatformIds[0], CL_DEVICE_TYPE_CPU, DeviceIdCount, DeviceIds.data(), nullptr);
    printf("Found %u Devices\n", DeviceIdCount);
#else // GPU
    clGetDeviceIDs(PlatformIds[0], CL_DEVICE_TYPE_GPU, 0, nullptr, &DeviceIdCount);
    std::vector<cl_device_id> DeviceIds(DeviceIdCount);
    clGetDeviceIDs(PlatformIds[0], CL_DEVICE_TYPE_GPU, DeviceIdCount, DeviceIds.data(), nullptr);
    printf("Found %u Devices\n", DeviceIdCount);
#endif

    const cl_context_properties ContextProperties[] =
    {
        CL_CONTEXT_PLATFORM,
        reinterpret_cast<cl_context_properties>(PlatformIds[0]),
        0,
        0
    };

    cl_int ErrorCode;
    cl_context Context = clCreateContext(ContextProperties, DeviceIdCount, DeviceIds.data(), nullptr, nullptr, &ErrorCode);

    srand(time(0));
    u32 ELEMENT_COUNT = 100000;

    cl_float2* PositionData = (cl_float2*)malloc(sizeof(cl_float2) * ELEMENT_COUNT);
#if 0
    std::fill_n(TestData, ELEMENT_COUNT, {5.0f, 5.0f});
#else
    for(int i=0;i<ELEMENT_COUNT;++i)
    {
        vec2 P = glm::linearRand(vec2(-400, -300), vec2(400, 300));
        // Init here
        PositionData[i].s[0] = P.x;
        PositionData[i].s[1] = P.y;
    }
#endif

    cl_float2* ResultData = (cl_float2*)malloc(sizeof(cl_float2) * ELEMENT_COUNT);
    cl_float2* MovementData = (cl_float2*)malloc(sizeof(cl_float2) * ELEMENT_COUNT);
    cl_float2* MovementData2 = (cl_float2*)malloc(sizeof(cl_float2) * ELEMENT_COUNT);
    for(int i=0;i<ELEMENT_COUNT;++i)
    {
        vec2 DirToCenter = glm::normalize(-vec2(PositionData[i].s[0], PositionData[i].s[1]));
#if 0
        vec2 MV = glm::circularRand(1.0f);
#else
        vec2 MV = glm::gaussRand(vec2(-1.0f, -1.0f), vec2(1.0f, 1.0f));
#endif
        MovementData[i].s[0] = DirToCenter.x;
        MovementData[i].s[1] = DirToCenter.y;
    }



    u32 DataSize = ELEMENT_COUNT * sizeof(cl_float2);

    // Allocate Buffer
    cl_mem Buffer = clCreateBuffer(Context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, DataSize, PositionData, &ErrorCode);
    cl_mem ResultBuffer = clCreateBuffer(Context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, DataSize, ResultData, &ErrorCode);
    cl_mem MovementBuffer = clCreateBuffer(Context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, DataSize, MovementData, &ErrorCode);
    cl_mem MovementBuffer2 = clCreateBuffer(Context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, DataSize, MovementData2, &ErrorCode);

    cl_command_queue CommandQueue = clCreateCommandQueue(Context, DeviceIds[0], 0, &ErrorCode);
    clCheckError(ErrorCode);

    clEnqueueWriteBuffer(CommandQueue, Buffer, CL_TRUE, 0, DataSize, PositionData, 0, nullptr, nullptr);

    std::string ProgramSource = LoadKernel("kernels/Testkernel.cl");


    size_t SourceLength[1] = { ProgramSource.size() };
    const char* ProgramSourceArr[1] = { ProgramSource.data() };

    cl_program Program = clCreateProgramWithSource(Context, 1, ProgramSourceArr, SourceLength, &ErrorCode);
    clCheckError(ErrorCode);

    clCheckError(clBuildProgram(Program, DeviceIdCount, DeviceIds.data(), nullptr, nullptr, nullptr));
    cl_kernel ProgramKernel = clCreateKernel(Program, "TestKernel", &ErrorCode);
    clCheckError(ErrorCode);


    clSetKernelArg(ProgramKernel, 0, sizeof(cl_mem), &Buffer);
    clSetKernelArg(ProgramKernel, 1, sizeof(cl_mem), &ResultBuffer);
    clSetKernelArg(ProgramKernel, 2, sizeof(cl_mem), &MovementBuffer);
    clSetKernelArg(ProgramKernel, 3, sizeof(cl_mem), &MovementBuffer2);

    //@SDL
    sdl2 SDL = InitSDL_GL_Window(800, 600);
    bool32 IsRunning = true;
    SDL_Event Event;

    mat4 Projection = glm::ortho<f32>(-400, 400, -300, 300);
    glMatrixMode(GL_PROJECTION_MATRIX);
    glLoadMatrixf(glm::value_ptr(Projection));
    glPointSize(3.0f);

    while(IsRunning)
    {
        while(SDL_PollEvent(&Event))
        {
            switch(Event.type)
            {
            case SDL_QUIT:
                IsRunning = false;
                break;
            case SDL_WINDOWEVENT_RESIZED:
                break;
            }
        }
        clSetKernelArg(ProgramKernel, 0, sizeof(cl_mem), &Buffer);
        clSetKernelArg(ProgramKernel, 1, sizeof(cl_mem), &ResultBuffer);
        clSetKernelArg(ProgramKernel, 2, sizeof(cl_mem), &MovementBuffer);
        clSetKernelArg(ProgramKernel, 3, sizeof(cl_mem), &MovementBuffer2);

        const size_t GlobalWorkSize[] = { ELEMENT_COUNT, 0, 0 };
        clEnqueueNDRangeKernel(CommandQueue, ProgramKernel, 1, nullptr, GlobalWorkSize, nullptr, 0, nullptr, nullptr);

        clEnqueueReadBuffer(CommandQueue, ResultBuffer, CL_TRUE, 0, DataSize, ResultData, 0, nullptr, nullptr);
        clEnqueueReadBuffer(CommandQueue, MovementBuffer2, CL_TRUE, 0, DataSize, MovementData, 0, nullptr, nullptr);


        clFlush(CommandQueue);
        cl_mem Tmp = Buffer;
        Buffer = ResultBuffer;
        ResultBuffer = Tmp;

        Tmp = MovementBuffer2;
        MovementBuffer2 = MovementBuffer;
        MovementBuffer = Tmp;

        glClearColor(0.333f, 0.333f, 0.333f, 1.0f);
        glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);

        glBegin(GL_POINTS);
            for(int i=0;i<ELEMENT_COUNT;++i)
            {
                glColor4f(
                    (MovementData[i].s[0] / 2.0f) + .5f,
                    (MovementData[i].s[1] / 2.0f) + .5f,
                    0.0f,
                    1.0f
                );
                glVertex2f(ResultData[i].s[0], ResultData[i].s[1]);
            }
        glEnd();

        SDL_GL_SwapWindow(SDL.Window);
    }

#if 0
    std::cout << "Start calculation" << std::endl;
    clock_t Begin = clock();

    for(int i=0;i<1;++i)
    {
        clSetKernelArg(ProgramKernel, 0, sizeof(cl_mem), &Buffer);
        clSetKernelArg(ProgramKernel, 1, sizeof(cl_mem), &ResultBuffer);

        const size_t GlobalWorkSize[] = { ELEMENT_COUNT, 0, 0 };
        clEnqueueNDRangeKernel(CommandQueue, ProgramKernel, 1, nullptr, GlobalWorkSize, nullptr, 0, nullptr, nullptr);

        clEnqueueReadBuffer(CommandQueue, ResultBuffer, CL_TRUE, 0, DataSize, ResultData, 0, nullptr, nullptr);


        clFlush(CommandQueue);
        cl_mem Tmp = Buffer;
        Buffer = ResultBuffer;
        ResultBuffer = Tmp;
    }
    clock_t End = clock();


#if 1
    for(int i=0;i<ELEMENT_COUNT;++i)
    {
        printf("(%3.2f, %3.2f) -> (%3.2f, %3.2f) \n", TestData[i].s[0], TestData[i].s[1], ResultData[i].s[0], ResultData[i].s[1]);
    }
#endif
    std::cout << "Calculation done in " << (f32(End - Begin) / CLOCKS_PER_SEC)*1000.0f << "ms" << std::endl;
#endif

    SDL_DestroyWindow(SDL.Window);
    SDL_Quit();

    //@Cleanup
    clReleaseCommandQueue(CommandQueue);
    clReleaseMemObject(Buffer);
    clReleaseKernel(ProgramKernel);
    clReleaseProgram(Program);
    clReleaseContext(Context);

    return 0;
}
