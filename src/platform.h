#ifndef PLATTFORM_H
#define PLATTFORM_H

#include <stdint.h>
#include <glm/glm.hpp>

typedef int8_t      int8;
typedef int16_t     int16;
typedef int32_t     int32;
typedef int64_t     int64;

typedef int8_t      s8;
typedef int16_t     s16;
typedef int32_t     s32;
typedef int64_t     s64;

typedef uint8_t     uint8;
typedef uint16_t    uint16;
typedef uint32_t    uint32;
typedef uint64_t    uint64;

typedef uint8_t     u8;
typedef uint16_t    u16;
typedef uint32_t    u32;
typedef uint64_t    u64;

typedef int32_t     bool32;

typedef float       f32;
typedef double      f64;

typedef glm::vec2   vec2;
typedef glm::vec3   vec3;
typedef glm::vec4   vec4;

typedef glm::quat   quat;

typedef glm::mat3   mat3;
typedef glm::mat4   mat4;

typedef glm::vec3   rgb;
typedef glm::vec4   rgba;

static_assert(sizeof(int8) == 1, "int8 not 1 byte");
static_assert(sizeof(int16) == 2, "int16 not 2 byte");
static_assert(sizeof(int32) == 4, "int32 not 4 byte");
static_assert(sizeof(int64) == 8, "int64 not 8 byte");

static_assert(sizeof(uint8) == 1, "uint8 not 1 byte");
static_assert(sizeof(uint16) == 2, "uint16 not 2 byte");
static_assert(sizeof(uint32) == 4, "uint32 not 4 byte");
static_assert(sizeof(uint64) == 8, "int64 not 8 byte");

/** more explicit shorthands **/
#define scope_persist static
#define global_variable static
#define pw_internal static

#endif // PLATTFORM_H

