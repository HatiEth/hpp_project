#include "platformlayer.h"
#include <iostream>
#include <SDL2/SDL.h>
#include <cassert>

sdl2 InitSDL_GL_Window(int windowWidth, int windowHeight)
{
    sdl2 sdl;

#ifdef _PW_DEBUG_
    std::setbuf(stdout, NULL); // used to always flush std::cout
#endif

    auto sdl_init_status = SDL_Init(SDL_INIT_EVENTS
                                     | SDL_INIT_JOYSTICK
                                     | SDL_INIT_GAMECONTROLLER);
    assert(sdl_init_status==0);
#if __unix__
    sdl.Window = SDL_CreateWindow(PW_WINDOW_TITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowWidth, windowHeight, SDL_WindowFlags::SDL_WINDOW_SHOWN|SDL_WindowFlags::SDL_WINDOW_OPENGL);
#else
    sdl.sdlWindow = SDL_CreateWindow(PW_WINDOW_TITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowWidth, windowHeight, SDL_WindowFlags::SDL_WINDOW_SHOWN|SDL_WindowFlags::SDL_WINDOW_OPENGL|SDL_WindowFlags::SDL_WINDOW_RESIZABLE);
#endif

    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
    sdl.sdl_glContext = SDL_GL_CreateContext(sdl.Window);

    return sdl;
}
