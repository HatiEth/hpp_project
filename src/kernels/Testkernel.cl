__kernel void TestKernel(__global float2* x, __global float2* y, __global float2* MV, __global float2* NMV)
{
    const int i = get_global_id(0);
    
    y[i].x = x[i].x + MV[i].x * 0.16f;
    y[i].y = x[i].y + MV[i].y * 0.16f;

    //NMV[i].x = (MV[i].y * MV[i].x);
    NMV[i].x = MV[i].x;
    NMV[i].y = MV[i].y;
}
